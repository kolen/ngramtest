(ns test.core
  (:gen-class))

(def order 4)

(defn learn-window [words ngrams]
  (let [from (pop words) to (peek words) existing (ngrams from)]
    (if existing
      (assoc ngrams from (conj existing to))
      (assoc ngrams from #{to})
  )))

(defn add-start-and-terminate [words]
  (concat (repeat (- order 1) nil) words '(nil)))

(defn learn [words ngrams]
  (loop [windows (partition order 1 (add-start-and-terminate words)) ngrams ngrams]
    (if (seq windows) (recur (rest windows) (learn-window (vec (first windows)) ngrams))
      ngrams
      )
    )
  )

(defn generate-next [window ngrams]
  (let [next (rand-nth (seq (ngrams window)))]
    '(vector (concat (subvec window 1) '(next)) next)
    )
  )

(defn slide-window [window word]
  (conj (subvec window 1) word))

(defn generate-sentence [ngrams]
  (loop [generated (vector) window (vec (repeat (- order 1) nil))]
    (let [next (rand-nth (seq (ngrams window)))]
      (if next
        (recur (conj generated next) (slide-window window next))
        generated
        )
      )
    )
  )

(defn parse-string [string]
  (clojure.string/split string #"\s+"))

(defn learn-text [text model]
  (learn (parse-string text) model)
  )

(defn learn-texts [texts model]
  (if (seq texts)
    (recur (rest texts) (learn-text (first texts) model))
    model)
  )

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  ;; work around dangerous default behaviour in Clojure
  (alter-var-root #'*read-eval* (constantly false))
  (let [model (learn-texts ["Повеситься в гроб" "Жмур накрылся тазом и выкатил" "Висельник накрылся тазом и захрипел"] {} )]
    (println model)
    (println (generate-sentence model))
    )

  )

